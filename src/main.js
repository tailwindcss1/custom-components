import Vue from 'vue'
import Navigation from '../src/components/Navigation';

Vue.config.productionTip = false



new Vue({
  render: h => h(Navigation),
}).$mount('#navigation');
